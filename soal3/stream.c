#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <json-c/json.h>
#include <ctype.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>

#define MAX_STRING_LENGTH 1024

/* Define Struct */

/* Struct for Message Queue */
struct my_message {     // Struct for the message queue
    long int mtype;     // Store the message type
    pid_t sender_pid;   // the PID of the sender process
    char mtext[MAX_STRING_LENGTH];   // Store the message string
} message;

/* Struct for Decrypting song-olaylist.json */
struct song {
    char method[32];
    char song[256];
};

/* Function to Decode Hex Encryption from song-playlist.json */
void decryptHex(char *str){
    char string[MAX_STRING_LENGTH];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}


/* Function to Decode Base64 Encryption from song-playlist.json */
char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // Initialize the
                    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',  // Base 64 Character Map
                    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* decryptBase64(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc((strlen(cipher) * 3 / 4) + 64);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {                            // Start to decrypting the string
        char k;
        for(k = 0 ; k < 64 && base64_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    strcpy(cipher, plain);
    return plain;
}

/* Function to Decrypt rot13 encryption from song-playlist.json */
void decryptrot13(char *str){
    char c;
    while(*str){
        c = *str;
        if(isalpha(c)){
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M'){
                *str += 13;
            } else{
                *str -= 13;
            }
        }
        str++;
    }
}

/* Function to decrypt song-playlist.json */
void decryptJSON(bool isAlreadyDecrypted) {
    
    /* Check Whether the .json files is already decrypted */
    if(isAlreadyDecrypted == true){
        printf("Already Decoded!\n");
        return;
    }

    const char *fp = "song-playlist.json"; // initialize the files name to decrypt

    struct json_object *parsed_json;       // Initialize the json object (Handled by json-c extension)
    
    parsed_json = json_object_from_file(fp);    // Open the json files

    /* Checking the json content */
    if(!json_object_is_type(parsed_json, json_type_array)){     // If the json content is not an array, it will return immediately
        printf("JSON is not an array!\n");                      // Print if the json is not an array
        return;
    }

    int array_len = json_object_array_length(parsed_json);      // Initialize the length of object array from json

    for(int i = 0; i < array_len; i++){
        struct json_object *item = json_object_array_get_idx(parsed_json, i);
        if(!json_object_is_type(item, json_type_object)){
            printf("Array item is not an object!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(item, "method", &method)){
            printf("No method in object!\n");
            continue;
        }

        if(!json_object_object_get_ex(item, "song", &song)){
            printf("No song in object!\n");
            continue;
        }

        //printf("Method: %s\nSong: %s\n", json_object_get_string(method), json_object_get_string(song));

        if (strstr(json_object_get_string(method), "hex") != NULL) {
            //printf("The method is Hex.\n");
            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptHex(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);

            char command[strlen(decryptedSong) + 64];

            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);

        } else if(strstr(json_object_get_string(method), "base64") != NULL){ /* BASE64 */
            //printf("The method is base64\n");

            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptBase64(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);

            char command[strlen(decryptedSong) + 64];

            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);

        } else if (strstr(json_object_get_string(method), "rot13") != NULL){ /* ROT 13 */
            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptrot13(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);
            char command[strlen(decryptedSong) + 64];
            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);
        } else {
            printf("UNKNOWN METHOD!\n");
        }

        //printf("\n");
        
    }
    
    json_object_put(parsed_json);
    system("sort -u playlist.txt -o playlist.txt"); // Sort content of playlist in ascending order
    printf("Berhasil Decrypt\n");   // Output if decrypting is finish
}

/* Function to list all song in playlist.txt */
void listSong() {
    //printf("List Lagu\n");  
    system("cat playlist.txt");     // Using system cat to Concatenate the content of playlist.txt
}

/* Function to compare substring with insensitive case */
char *strcasestr(const char *haystack, const char *needle) {
    const char *p = haystack;
    size_t needle_len = strlen(needle);

    while (*p != '\0') {
        if (strncasecmp(p, needle, needle_len) == 0) {
            return (char *)p;
        }
        p++;
    }

    return NULL;
}

/* Function to play song if listed on playlist.txt */
void playSong(char *songTitle) {
	FILE *fp;
    char *fname = "playlist.txt";
	int line_num = 1;       // Initialize the variable to count and increment to output
	int find_result = 0;    // Initialize the variable to count how many song that matched the substring given by user
	char temp[512];
	
    fp = fopen(fname, "r"); // Open filename (in this case is playlist.txt)

    /* Loop to search how many substring that matched in playlist.txt */
	while(fgets(temp, 512, fp) != NULL) {
		if((strcasestr(temp, songTitle)) != NULL) {
			find_result++;
		}
	}

    if(fp) {
		fclose(fp);     // close stream files
	}

    /* Check the result */
	if(find_result == 0) {
		printf("THERE IS NO SONG CONTAINING \"%s\"\n", songTitle);      // If there are no song contained in playlist
	} else if (find_result == 1){   
        //printf("Result Found : %d\n", find_result);
        fp = fopen(fname, "r");
        printf("USER \"%d\" IS PLAYING ", message.sender_pid);          // If there are only 1 song matched in playlist.txt
        /* Loop to search the song */
		while(fgets(temp, 512, fp) != NULL) {
            if((strcasestr(temp, songTitle)) != NULL) {     // Check whether the substring matched the song
                if ((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n'))     // To delete the "newline" character if exist and replace it with null character in string
                    temp[strlen (temp) - 1] = '\0';
                printf("\"%s\"\n", temp);
                line_num++;
            }
        }
        if(fp) {
		    fclose(fp); // Close stream files
	    }

    } else {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\" :\n", find_result, songTitle);  // If there are >1 song matched in playlist.txt
        fp = fopen(fname, "r");     // Open stream files
        char temp[512];

        /* Loop to list all the song */
		while(fgets(temp, 512, fp) != NULL) {       // Check whether the playlist matched the song
            if ((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) // To delete the "newline" character if exist and replace it with null character in string
                    temp[strlen (temp) - 1] = '\0';
            if((strcasestr(temp, songTitle)) != NULL) {
                printf("%d. %s\n", line_num, temp);     // Print songs that matched the substring given by user
                line_num++;     // Increment the list number
            }
        }
        if(fp) {
		    fclose(fp);     // Close Stream
	    }
        
    }
	
   	return;
}

/* Function to add song into playlist by user */
void addSong(char *songTitle) {
    //printf("Menambahkan Lagu %s Ke Playlist\n", songTitle); 
    FILE *fp;
    char *fname = "playlist.txt";
	int find_result = 0;
	char temp[512];
	
    fp = fopen(fname, "r");     // Open filestream
    
    /* Loop to search whether there is any song given by user that matched in playlist */
	while(fgets(temp, 512, fp) != NULL) {
        if ((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n'))
                    temp[strlen (temp) - 1] = '\0';
		if((!strcasecmp(temp, songTitle))) {    // Using strcasecmp to compare between 2 case-insensitive string
			find_result++;
		}
	}

    /* Check the result */
	if(find_result == 0) {      // If there are no song matched in playlist
		printf("USER \"%d\" ADD \"%s\"\n", message.sender_pid, songTitle);
        char command[strlen(songTitle) + 32];
        snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", songTitle);      // Insert the given song into the playlist
        system(command);
	} else {    // If there is any song matched in playlist
        printf("SONG ALREADY ON PLAYLIST");     // Output
    }
	
    system("sort -u playlist.txt -o playlist.txt"); // Sort playlist.txt into ascending order
    
    if(fp) {
		fclose(fp);
	}
   	return;
    
}

int main() {
    key_t key = ftok("my_queue", 45);   // Generate token for message queue
    int msgid = msgget(key, IPC_CREAT | 0666);  // Get message id for message queue
    char dummy[MAX_STRING_LENGTH];  

    bool isAlreadyDecypted = false;     // Check if the song-playlist.json already decrypted

    /* Semaphore */
    sem_unlink("/receive");
    sem_unlink("/write");

    sem_t *receiver = sem_open("/receive", O_WRONLY | O_CREAT | O_EXCL, 0666, 0);   // New semaphore for receiver
    if(receiver == SEM_FAILED){
        perror("sem_open/receive");
        exit(EXIT_FAILURE);
    }
    sem_t *writer = sem_open("/write", O_WRONLY | O_CREAT | O_EXCL, 0666, 2);       // New semaphore for user
    if(writer == SEM_FAILED){
        perror("sem_open/write");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // receive message
        sem_wait(receiver);     

        msgrcv(msgid, &message, sizeof(message), 0, 0);
        printf("Received Message : %s\n", message.mtext);

        /* Check User Input */
        if(!strcmp(message.mtext, "DECRYPT")){      // If the user input DECRYPT
            decryptJSON(isAlreadyDecypted);
            isAlreadyDecypted = true;
        } else if (!strcmp(message.mtext, "LIST")){     // If the user input LIST
            listSong();
        } else if (strstr(message.mtext, "ADD")){   // If the user input ADD
            int len = strlen(message.mtext);
            char songTitle[len - 3];
            strncpy(songTitle, message.mtext + 4, len - 1);     // Extract only the songTitle given by user
            addSong(songTitle);
        } else if (strstr(message.mtext, "PLAY")){  // If the user input PLAY
            int len = strlen(message.mtext);       
            char songTitle[len - 4];
            strncpy(songTitle, message.mtext + 5, len - 1);      // Extract only the songTitle given by user
            playSong(songTitle);
        } else {
            printf("UNKNOWN COMMAND\n");        // If the user input is unknowned by stream process
        }
        printf("\n");
        sem_post(writer);
    }
    return 0;
}
