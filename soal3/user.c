#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/shm.h>

struct my_message {
    long int mtype;     
    pid_t sender_pid;
    char mtext[1024];
} message;

int main() {
    key_t key = ftok("my_queue", 45);   
    int msgid = msgget(key, IPC_CREAT | 0666);    

    /* Semaphore */
    sem_t *receiver = sem_open("/receive", 0);
    if(receiver == SEM_FAILED){
        perror("sem_open/receive");
        exit(EXIT_FAILURE);
    }
    sem_t *writer =  sem_open("/write", 0);
    if(writer == SEM_FAILED){
        perror("sem_open/write");
        exit(EXIT_FAILURE);
    }

    int i = 1;
    
    int con;
    sem_getvalue(writer, &con);
    if(!con) {
        printf("STREAM SYSTEM OVERLOAD\n");
    }

    while (1) {
        sem_wait(writer);

        message.mtype = i;

        printf("masukkan Perintah : ");   
        fgets(message.mtext, 1024, stdin); 

        if ((strlen(message.mtext) > 0) && (message.mtext[strlen(message.mtext) - 1] == '\n')) 
            message.mtext[strlen (message.mtext) - 1] = '\0';                                   
        message.sender_pid = getpid();      
        msgsnd(msgid, &message, sizeof(message), 0);       
        printf("Sent message: %s\n", message.mtext);       
        
        i++;
        sem_post(receiver);
    }
    return 0;
}
