# sisop-praktikum-modul-3-2023-am-b07



## Soal 1

Pertama dapat kita download dulu file.txt

```
    //download file terlebih dahulu
	char link[] = "https://drive.google.com/u/0/uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download";
	system("wget 'https://drive.google.com/u/0/uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download' -O file.txt");
```

![Download](soal1/download.png)

Pada soal dapat kita hitung banyak frekuensi huruf dalam file.txt
```
                system("tr a-z A-Z < file.txt > uppercase_file.txt");
                system("tr -cd '[:alpha:]' < uppercase_file.txt > new.txt");
                int freq[26] = {0};
		        //hitung frekuensi
                countLetter("new.txt", freq);

```

Kemudian dapat kita send ke child process
```
        //tulis freq ke child
		write(fd1[1], &freq, sizeof(freq));
		close(fd1[1]);
```

Kemudian kita lakukan kompresi di child process setelah membaca frekuensi dan membuat huffman tree serta huffman table
```
        // Read a string using first pipe
		int freq[26];
		read(fd1[0], &freq, sizeof(freq));

		int count = 0;
		for(int i = 0; i < 26; i++)
                {
                        if(freq[i])count++;
                }

		int j = 0;
		int freq2[count];
                char alpha[count];

		//mencari huruf mana yang terdapat pada text
		for(int i = 0; i < 26; i++)
		{
			if(freq[i])
			{
				freq2[j] = freq[i];
				alpha[j] = 'A' + i;
				j++;
			}
		}


		//membuat huffman tree
		int size = sizeof(alpha) / sizeof(alpha[0]);
		struct MinHeapNode* root = buildHuffmanTree(alpha, freq2, size);
		int arr[MAX_TREE_HT], top = 0;

		printf("Size: %d\n", size);
		printf("Compressing File\n");


		int table[size];
		int temp = size;

		//membuat huffman table
		for (int i = 0; i < size; i++) {
        		table[i] = -1; // Initialize table entries to -1
    		}
    		buildHuffmanTableRecursive(root, table, 0, 0);

		//compress table
		int tablesize = sizeof(table) / sizeof(table[0]);
		compressFile("new.txt", "compressed.txt", table, tablesize);
```

Hasil huffman :
![Huffman](soal1/huffman.png)

Setelah itu kita send lagi huffman table ke parent process
```
        write(fd2[1], &tablesize, sizeof(int));
		write(fd2[1], &table, sizeof(int) * tablesize);
```

Di parent process tinggal kita baca table dan decompress file
```
        //baca size table
		int size = 0;
		read(fd2[0], &size, sizeof(int));

		//read huffman table
		int table[size];
		read(fd2[0], &table, sizeof(int) * size);

		//decompress file
		decompressFile("compressed.txt", "decompressed.txt", table, size);
```

Ada beberapa kendala yang ditemukan saat mengerjakan soal ini : 
1. Table yang di send tidak di dapat di read di parent process. Setelah demo dan mencari lebih lanjut, masih tidak diketahui mengapa hal tersebut terjadi.
2. Compression function masih salah, tidak berhasil menemukan cara compress yang benar menggunakan implementasi sekarang.

Dikarenakan kendala di atas perbandingan bit tidak dapat dilakukan

## Soal 2

Untuk program kalian.c, kita diminta menampilkan hasil perkalian matrix 4x2 dengan isi bilangan random di range 1-5 inklusif dengan matrix 2x5 di range 1-4 inklusif. Untuk bisa melakukan hal tersebut kita bisa membuat fungsi yang dapat menhasilkan angka random berdasarkan batas atas dan bawah yang diberikan. Fungsi tersebut adalahs sebagai berikut :

```
int generateRandom(int lower, int upper)
{
	return (rand() % (upper - lower + 1)) + lower;
}
```

Kemudian karena kita ingin menggunakan memory ini nilai matrix ini nantinya, kita bisa mengattach memori matrix ke processor.

```
    key_t key = 1234;
	int matrix1[4][2];
	int matrix2[2][5];
	int (*matrixres)[5];

	int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
	matrixres = shmat(shmid, NULL, 0);
	//isi matrix 1
	srand(time(0));
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			matrix1[i][j] = generateRandom(1, 5);
		}
	}

        //isi matrix 2
        for(int i = 0; i < 2; i++)
        {
                for(int j = 0; j < 5; j++)
                {
                        matrix2[i][j] = generateRandom(1, 4);
                }
        }

	//perkalian matrix
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 5; j++)
		{
			matrixres[i][j] = 0;
			for(int k = 0; k < 2; k++)
			{
				matrixres[i][j] += matrix1[i][k] * matrix2[k][j];
			}
		}
	}

```

Di kodetersebut terdapat `srand(time(0));` yang mana merupakan menggunakan waktu sekarang sebagai seed randomize number. Sehingga angka yang didapatkan selalu random untuk setiap runnning file kalian.c. Setelah kita mengkalikan matrix, dapat kita tampilkan dengan kode : 

```
printf("Hasil Perkalian Program kalian.c : \n");
	printf("[");
	for(int i = 0; i < 4; i++)
	{
		printf("[");
		for(int j = 0; j < 5; j++)
		{
			printf("%d", matrixres[i][j]);
			if(j != 4)printf(", ");
		}
		printf("]");
		if(i != 3)printf(", ");
	}
	printf("]\n");
	shmdt(matrixres);

```

Kode `shmdt(matrixres);` akan mendetach matrixres di program kalian.c dari memori.


Selanjutnya kita bisa lanjut ke cinta.c yang mana akan mengambil isi memori yang dari program kalian.c tadi dan mencari nilai faktorial untuk seluruh nilai di matrix menggunakan threading.

```
int main()
{
	key_t key = 1234;
	int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
	int (*res)[5];
	res = shmat(shmid, NULL, 0);

	printf("Hasil Matrix cinta.c : \n");
        printf("[");
        for(int i = 0; i < 4; i++)
        {
                printf("[");
                for(int j = 0; j < 5; j++)
                {
                        printf("%d", res[i][j]);
                        if(j != 4)printf(", ");
                }
                printf("]");
                if(i != 3)printf(", ");
        }
        printf("]\n");

	//hitung waktu
	clock_t t;
    	t = clock();


	//factorial
	pthread_t t_id[20];
	printf("Thread berhasil dibuat\n");

	int count = 0;
	for (int i=0 ; i<4; i++)
	{
		for(int j = 0; j < 5; j++)
		{
			int *num_to_check = (int *)malloc(sizeof(int));
			*num_to_check = res[i][j];
			pthread_create(&t_id[count++], NULL, &run, (void *)num_to_check);
		}
	}

	for (int i=0 ; i<20; i++) {
		pthread_join(t_id[i], NULL);
	}
	printf("\nThread telah selesai\n");

	t = clock() - t;
    	double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    	printf("Waktu menggunakan thread : %f detik\n", time_taken);

        shmdt(res);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}
```

Untuk template threading ini sendiri di ambil dari modul 3 sisop kemudian di modifikasi sedikit agar memenuhi syarat soal. Ditambah juga beberapa kode untuk menghitung elapsed time agar bisa di banding dengan sisop.c nantinya. Salah satu hal lagi yang beda adalah sebuah fungsi run yang akan menghitung nilai faktorial dari setiap angka. Fungsi run didefinisikan sebagai berikut :

```
void *run(void *args) {
	int angka;
	angka = *((int *)args);

	unsigned long long ans = 1;
	for(int i = 2; i <= angka; i++)
	{
		ans *= i;
	}
	printf("%llu ", ans);
        return NULL;
}
```

Di sisop.c yang berbeda dengan cinta.c hanyalah sisop.c tidak menggunakan thread untuk menghitung faktorial.

```
void factorial(int angka) {
	unsigned long long ans = 1;
	for(int i = 2; i <= angka; i++)
	{
		ans *= i;
	}
	printf("%llu ", ans);
}

int main()
{
	key_t key = 1234;
	int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
	int (*res)[5];
	res = shmat(shmid, NULL, 0);

	printf("Hasil Matrix sisop.c : \n");
        printf("[");
        for(int i = 0; i < 4; i++)
        {
                printf("[");
                for(int j = 0; j < 5; j++)
                {
                        printf("%d", res[i][j]);
                        if(j != 4)printf(", ");
                }
                printf("]");
                if(i != 3)printf(", ");
        }
        printf("]\n");

	//hitung waktu
	clock_t t;
    	t = clock();


	//factorial
	for (int i=0 ; i<4; i++)
	{
		for(int j = 0; j < 5; j++)
		{
			factorial(res[i][j]);
		}
	}
	printf("\nProgram telah selesai\n");

	t = clock() - t;
    	double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    	printf("Waktu tidak menggunakan thread : %f detik\n", time_taken);

        shmdt(res);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}
```
Untuk hasil runnya bisa dilihat sebagai berikut (kode di update sedemikian rupa agar matrix cinta.c dan sisop dan c memiliki nilaiyang sama)

kalian.c :

![kalian](soal2/kalian.png)

cinta.c :

![cinta](soal2/cinta.png)

sisop.c : 
![sisop](soal2/sisop.png)

Bisa dilihat bahwa program yang tidak menggunakan threading lebih cepat (sisop.c) dibanding yang menggunakan threading (cinta.c).

## Soal 3

Struct untuk message queue dala user.c
```
struct my_message {
    long int mtype;     
    pid_t sender_pid;
    char mtext[1024];
} message;
```
Untuk mengirim message ke message queue sebagai berikut
```
sem_wait(writer);

        message.mtype = i;

        printf("masukkan Perintah : ");   
        fgets(message.mtext, 1024, stdin); 

        if ((strlen(message.mtext) > 0) && (message.mtext[strlen(message.mtext) - 1] == '\n')) 
            message.mtext[strlen (message.mtext) - 1] = '\0';                                   
        message.sender_pid = getpid();      
        msgsnd(msgid, &message, sizeof(message), 0);       
        printf("Sent message: %s\n", message.mtext);       
        
        i++;
        sem_post(receiver);
```
Melakukan validasi message input
```
if(!strcmp(message.mtext, "DECRYPT")){      // If the user input DECRYPT
            decryptJSON(isAlreadyDecypted);
            isAlreadyDecypted = true;
        } else if (!strcmp(message.mtext, "LIST")){     // If the user input LIST
            listSong();
        } else if (strstr(message.mtext, "ADD")){   // If the user input ADD
            int len = strlen(message.mtext);
            char songTitle[len - 3];
            strncpy(songTitle, message.mtext + 4, len - 1);     // Extract only the songTitle given by user
            addSong(songTitle);
        } else if (strstr(message.mtext, "PLAY")){  // If the user input PLAY
            int len = strlen(message.mtext);       
            char songTitle[len - 4];
            strncpy(songTitle, message.mtext + 5, len - 1);      // Extract only the songTitle given by user
            playSong(songTitle);
        } else {
            printf("UNKNOWN COMMAND\n");        // If the user input is unknowned by stream process
        }
```
Melakukan decrypt menggunakan <json-c/json.h> sebagai berikut
```
const char *fp = "song-playlist.json"; // initialize the files name to decrypt

    struct json_object *parsed_json;       // Initialize the json object (Handled by json-c extension)
    
    parsed_json = json_object_from_file(fp);    // Open the json files

    /* Checking the json content */
    if(!json_object_is_type(parsed_json, json_type_array)){     // If the json content is not an array, it will return immediately
        printf("JSON is not an array!\n");                      // Print if the json is not an array
        return;
    }

    int array_len = json_object_array_length(parsed_json);      // Initialize the length of object array from json

    for(int i = 0; i < array_len; i++){
        struct json_object *item = json_object_array_get_idx(parsed_json, i);
        if(!json_object_is_type(item, json_type_object)){
            printf("Array item is not an object!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(item, "method", &method)){
            printf("No method in object!\n");
            continue;
        }

        if(!json_object_object_get_ex(item, "song", &song)){
            printf("No song in object!\n");
            continue;
        }

        //printf("Method: %s\nSong: %s\n", json_object_get_string(method), json_object_get_string(song));

        if (strstr(json_object_get_string(method), "hex") != NULL) {
            //printf("The method is Hex.\n");
            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptHex(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);

            char command[strlen(decryptedSong) + 64];

            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);

        } else if(strstr(json_object_get_string(method), "base64") != NULL){ /* BASE64 */
            //printf("The method is base64\n");

            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptBase64(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);

            char command[strlen(decryptedSong) + 64];

            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);

        } else if (strstr(json_object_get_string(method), "rot13") != NULL){ /* ROT 13 */
            char decryptedSong[256];
            strcpy(decryptedSong, json_object_get_string(song));
            decryptrot13(decryptedSong);
            //printf("Decrypted Song : %s\n", decryptedSong);
            char command[strlen(decryptedSong) + 64];
            snprintf(command, sizeof(command), "echo \"%s\" >> playlist.txt", decryptedSong);
            system(command);
        } else {
            printf("UNKNOWN METHOD!\n");
        }
```
Melakukan decrypt dengan hex, base64, dan rot13
```
void decryptHex(char *str){
    char string[MAX_STRING_LENGTH];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}


/* Function to Decode Base64 Encryption from song-playlist.json */
char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // Initialize the
                    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',  // Base 64 Character Map
                    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* decryptBase64(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc((strlen(cipher) * 3 / 4) + 64);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {                            // Start to decrypting the string
        char k;
        for(k = 0 ; k < 64 && base64_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    strcpy(cipher, plain);
    return plain;
}

/* Function to Decrypt rot13 encryption from song-playlist.json */
void decryptrot13(char *str){
    char c;
    while(*str){
        c = *str;
        if(isalpha(c)){
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M'){
                *str += 13;
            } else{
                *str -= 13;
            }
        }
        str++;
    }
}
```
Untuk menjadikan command case-insensitive memakai code berikut
```
char *strcasestr(const char *haystack, const char *needle) {
    const char *p = haystack;
    size_t needle_len = strlen(needle);

    while (*p != '\0') {
        if (strncasecmp(p, needle, needle_len) == 0) {
            return (char *)p;
        }
        p++;
    }

    return NULL;
}
```
Struct untuk parsing json sebagai berikut
```
struct song {
    char method[32];
    char song[256];
};
```

## Soal 4

Source Code unzip.c:
```
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	system("curl -L \"https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp\" -o hehe.zip");
	system("unzip hehe.zip");

	return 0;
}
```
Poin pertama dari problem nomor 4 adalah mengunduh file dari Google Drive dan unzip. Di sini kita gunakan curl dengan parameter -L untuk melewati redirect dari Google Drive lalu dilakukan unzip.

Output:

![Unzip](soal4/output-unzip.png)

Source Code categorize.c:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    FILE *fp;
    FILE *fp_log;
    char extensions[100][100];
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    // Open extensions file
    fp = fopen(EXTENSIONS_FILE, "r");
    if (fp == NULL) {
        return 1;
    }

    // Open log file
    fp_log = fopen(LOG_FILE, "w");
    if (fp_log == NULL) {
        return 1;
    }

    fprintf(fp_log, "%s ACCESSED extensions.txt\n", datetime);

    mkdir("categorized", 0777);
    fprintf(fp_log, "%s MADE categorized\n", datetime);

    int i = 0;

    // Loop through lines in extensions file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Remove newline character
        line[strlen(line)-2] = '\0';

        strcpy(extensions[i], line);
        printf("%s\n", line);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        mkdir(line, 0777);
        fprintf(fp_log, "%s MADE categorized/%s\n", datetime, line);
        i++;
    }

    chdir("categorized");
    fprintf(fp_log, "%s ACCESSED categorized\n", datetime);
    mkdir("other", 0777);
    fprintf(fp_log, "%s MADE categorized/other\n", datetime);

    fclose(fp);

    chdir("..");

    DIR *dir;
    struct dirent *ent;
    char *src_dir = "/home/rep/Project/Bash/Shift-3/files";

    // Open source directory
    if ((dir = opendir(src_dir)) == NULL) {
        return 1;
    }
    fprintf(fp_log, "%s ACCESSED files\n", datetime);

    // Loop through files in source directory
    while ((ent = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (ent->d_name[0] == '.') {
            continue;
        }

        // Get file extension
        char *extension = strrchr(ent->d_name, '.');
        if (extension == NULL) {
            continue;
        }

        // Move file to destination directory
        char src[256];
        snprintf(src, sizeof(src), "%s/%s", src_dir, ent->d_name);

        char dest[256];
        snprintf(dest, sizeof(dest), "%s/%s/%s", "/home/rep/Project/Bash/Shift-3/categorized", extension + 1, ent->d_name);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        if (rename(src, dest)) {
            printf("Moved file: %s -> %s\n", src, dest);
            fprintf(fp_log, "%s MOVED %s > %s\n", datetime, src, dest);
        }
    }

    closedir(dir);

    return 0;
}
```
Pada categorize.c kita mengambil informasi waktu sekarang untuk digunakan nanti. Lalu kita membuka 2 file yaitu extensions.txt secara read dan log.txt secara write. Program akan menuliskan catatan log ke dalam log.txt setiap kali membuat, membuka, atau memindahkan file/direktori.

Kode di bawah:
```
while ((read = getline(&line, &len, fp)) != -1) {
        // Remove newline character
        line[strlen(line)-2] = '\0';

        strcpy(extensions[i], line);
        printf("%s\n", line);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        mkdir(line, 0777);
        fprintf(fp_log, "%s MADE categorized/%s\n", datetime, line);
        i++;
    }
```
digunakan untuk mengambil string per line di extensions.txt. Lalu, membuat direktori berdasarkan line tersebut..
Setelah itu, dilakukan pengecekan ke dalam direktori files.
```
while ((ent = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (ent->d_name[0] == '.') {
            continue;
        }

        // Get file extension
        char *extension = strrchr(ent->d_name, '.');
        if (extension == NULL) {
            continue;
        }

        // Move file to destination directory
        char src[256];
        snprintf(src, sizeof(src), "%s/%s", src_dir, ent->d_name);

        char dest[256];
        snprintf(dest, sizeof(dest), "%s/%s/%s", "/home/rep/Project/Bash/Shift-3/categorized", extension + 1, ent->d_name);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        if (rename(src, dest)) {
            printf("Moved file: %s -> %s\n", src, dest);
            fprintf(fp_log, "%s MOVED %s > %s\n", datetime, src, dest);
        }
```
Di sini file/direktori dengan awalan . akan di-skip. Lalu, kita gunakan _strrchr_ untuk mendapatkan char '.' paling akhir untuk mengambil ekstensi file. Setelah itu kita definisikan source path dan destination path. Lalu, kita gunakan _rename_ untuk memindahkan file sesuai direktori yang sesuai dengan extension-nya.

Di categorize.c ini kami tidak bisa menggunakan multithreading.

Output:

![Categorize](soal4/output-categorize.png)

Source Code logchecker.c:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    FILE *fp;
    char *line = NULL;
    char extensions[100][100];
    size_t len = 0;
    ssize_t read;
    int count = 0;

    // Open log file
    fp = fopen(LOG_FILE, "r");
    if (fp == NULL) {
        printf("Failed to open file: %s\n", LOG_FILE);
        return 1;
    }

    // Loop through lines in log file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Count occurrences of "ACCESSED"
        char *check = strstr(line, "ACCESSED");
        while (check != NULL) {
            count++;
            check = strstr(check + 1, "ACCESSED");
        }
    }

    printf("ACCESSED : %d\n", count);

    fclose(fp);

    DIR *dir;
    struct dirent *entry;
    struct stat filestat;
    char *dir_path = "categorized/";

    // Open directory
    dir = opendir(dir_path);
    if (dir == NULL) {
        return 1;
    }

    int i = 0;

    // Loop through directory entries
    while ((entry = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (entry->d_name[0] == '.') {
            continue;
        }
        // Construct full path of entry
        char full_path[256];
        snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

        strcpy(extensions[i], entry->d_name);

        // Print name of entry
        printf("%s\n", entry->d_name);
        i++;
    }

    system("tree categorized");

    closedir(dir);
    printf("\n");

    for(int j = 0; j < i-1; j++) {
        int dircount = 0;
        chdir("categorized");
        dir = opendir(extensions[j]);
        if (dir == NULL) {
            return 1;
        }

        // Loop through directory entries
        while ((entry = readdir(dir)) != NULL) {
            // Ignore files that start with .
            if (entry->d_name[0] == '.') {
                continue;
            }
            // Construct full path of entry
            char full_path[256];
            snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

            // Print name of entry
            printf("DIR %s : %s\n", extensions[j], entry->d_name);
            dircount++;
        }
        printf("\n%s : %d\n\n", extensions[j], dircount);
    }

    closedir(dir);

    return 0;
}
```
Di logchecker.c ini kita menghitung kata "ACCESSED" yang muncul di dalam log.txt, lalu membaca semua file yang ada di dalam direktori categorized. Setelah itu, menghitung file yang ada di dalam direktori extension-nya masing-masing.

Output:

![Logchecker](soal4/output-logchecker.png)
