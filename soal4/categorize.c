#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    FILE *fp;
    FILE *fp_log;
    char extensions[100][100];
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    // Open extensions file
    fp = fopen(EXTENSIONS_FILE, "r");
    if (fp == NULL) {
        return 1;
    }

    // Open log file
    fp_log = fopen(LOG_FILE, "w");
    if (fp_log == NULL) {
        return 1;
    }

    fprintf(fp_log, "%s ACCESSED extensions.txt\n", datetime);

    mkdir("categorized", 0777);
    fprintf(fp_log, "%s MADE categorized\n", datetime);

    int i = 0;

    // Loop through lines in extensions file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Remove newline character
        line[strlen(line)-2] = '\0';

        strcpy(extensions[i], line);
        printf("%s\n", line);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        mkdir(line, 0777);
        fprintf(fp_log, "%s MADE categorized/%s\n", datetime, line);
        i++;
    }

    chdir("categorized");
    fprintf(fp_log, "%s ACCESSED categorized\n", datetime);
    mkdir("other", 0777);
    fprintf(fp_log, "%s MADE categorized/other\n", datetime);

    fclose(fp);

    chdir("..");

    DIR *dir;
    struct dirent *ent;
    char *src_dir = "/home/rep/Project/Bash/Shift-3/files";

    // Open source directory
    if ((dir = opendir(src_dir)) == NULL) {
        return 1;
    }
    fprintf(fp_log, "%s ACCESSED files\n", datetime);

    // Loop through files in source directory
    while ((ent = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (ent->d_name[0] == '.') {
            continue;
        }

        // Get file extension
        char *extension = strrchr(ent->d_name, '.');
        if (extension == NULL) {
            continue;
        }

        // Move file to destination directory
        char src[256];
        snprintf(src, sizeof(src), "%s/%s", src_dir, ent->d_name);

        char dest[256];
        snprintf(dest, sizeof(dest), "%s/%s/%s", "/home/rep/Project/Bash/Shift-3/categorized", extension + 1, ent->d_name);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        if (rename(src, dest)) {
            printf("Moved file: %s -> %s\n", src, dest);
            fprintf(fp_log, "%s MOVED %s > %s\n", datetime, src, dest);
        }
    }

    closedir(dir);

    return 0;
}