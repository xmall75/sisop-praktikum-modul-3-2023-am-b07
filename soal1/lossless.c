#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<sys/wait.h>
#include<ctype.h>
#include<fcntl.h>

#define MAX_TREE_HT 100

// A Huffman tree node

struct MinHeapNode {
 
    // One of the input characters
    char data;
 
    // Frequency of the character
    unsigned freq;

    // Left and right child of this node
    struct MinHeapNode *left, *right;
};
 
// A Min Heap:  Collection of
// min-heap (or Huffman tree) nodes
struct MinHeap {
 
    // Current size of min heap
    unsigned size;
 
    // capacity of min heap
    unsigned capacity;
 
    // Array of minheap node pointers
    struct MinHeapNode** array;
};
 
// A utility function allocate a new
// min heap node with given character
// and frequency of the character
struct MinHeapNode* newNode(char data, unsigned freq)
{
    struct MinHeapNode* temp = (struct MinHeapNode*)malloc(
        sizeof(struct MinHeapNode));
 
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
 
    return temp;
}
 
// A utility function to create
// a min heap of given capacity
struct MinHeap* createMinHeap(unsigned capacity)
 
{
 
    struct MinHeap* minHeap
        = (struct MinHeap*)malloc(sizeof(struct MinHeap));
 
    // current size is 0
    minHeap->size = 0;
 
    minHeap->capacity = capacity;
 
    minHeap->array = (struct MinHeapNode**)malloc(
        minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}
 
// A utility function to
// swap two min heap nodes
void swapMinHeapNode(struct MinHeapNode** a,
                     struct MinHeapNode** b)
 
{
 
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
 
// The standard minHeapify function.
void minHeapify(struct MinHeap* minHeap, int idx)
 
{
 
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
 
    if (left < minHeap->size
        && minHeap->array[left]->freq
               < minHeap->array[smallest]->freq)
        smallest = left;
 
    if (right < minHeap->size
        && minHeap->array[right]->freq
               < minHeap->array[smallest]->freq)
        smallest = right;
 
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
 
// A utility function to check
// if size of heap is 1 or not
int isSizeOne(struct MinHeap* minHeap)
{
 
    return (minHeap->size == 1);
}
 
// A standard function to extract
// minimum value node from heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
 
{
 
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
 
    --minHeap->size;
    minHeapify(minHeap, 0);
 
    return temp;
}
 
// A utility function to insert
// a new node to Min Heap
void insertMinHeap(struct MinHeap* minHeap,
                   struct MinHeapNode* minHeapNode)
 
{
 
    ++minHeap->size;
    int i = minHeap->size - 1;
 
    while (i
           && minHeapNode->freq
                  < minHeap->array[(i - 1) / 2]->freq) {
 
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
 
    minHeap->array[i] = minHeapNode;
}
 
// A standard function to build min heap
void buildMinHeap(struct MinHeap* minHeap)
 
{
 
    int n = minHeap->size - 1;
    int i;
 
    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}
 
// A utility function to print an array of size n
void printArr(int arr[], int n)
{
    int i;
    for (i = 0; i < n; ++i)
        printf("%d", arr[i]);
 
    printf("\n");
}
 
// Utility function to check if this node is leaf
int isLeaf(struct MinHeapNode* root)
 
{
 
    return !(root->left) && !(root->right);
    // Assign 1 to right edge and recur
    if (root->right);
}
 
// Creates a min heap of capacity
// equal to size and inserts all character of
// data[] in min heap. Initially size of
// min heap is equal to capacity
struct MinHeap* createAndBuildMinHeap(char data[],
                                      int freq[], int size)
 
{
 
    struct MinHeap* minHeap = createMinHeap(size);
 
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
 
    minHeap->size = size;
    buildMinHeap(minHeap);
 
    return minHeap;
}
 
// The main function that builds Huffman tree
struct MinHeapNode* buildHuffmanTree(char data[],
                                     int freq[], int size)
 
{
    struct MinHeapNode *left, *right, *top;
 
    // Step 1: Create a min heap of capacity
    // equal to size.  Initially, there are
    // modes equal to size.
    struct MinHeap* minHeap
        = createAndBuildMinHeap(data, freq, size);
 
    // Iterate while size of heap doesn't become 1
    while (!isSizeOne(minHeap)) {
 
        // Step 2: Extract the two minimum
        // freq items from min heap
        left = extractMin(minHeap);
        right = extractMin(minHeap);
 
        // Step 3:  Create a new internal
        // node with frequency equal to the
        // sum of the two nodes frequencies.
        // Make the two extracted node as
        // left and right children of this new node.
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not
        // used
        top = newNode('$', left->freq + right->freq);
 
        top->left = left;
        top->right = right;
 
        insertMinHeap(minHeap, top);
    }
 
    // Step 4: The remaining node is the
    // root node and the tree is complete.
    return extractMin(minHeap);
}
 
// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct MinHeapNode* root, int arr[],
                int top, int code[], int* l, char ch)
 
{
    // Assign 0 to left edge and recur
    if (root->left) {
 	//printf("go left\n");
        arr[top] = 0;
        printCodes(root->left, arr, top + 1, code, l, ch);
    }
 
    // Assign 1 to right edge and recur
    if (root->right) {
 	//printf("go right\n");
        arr[top] = 1;
        printCodes(root->right, arr, top + 1, code, l, ch);
    }
 
    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root)) {
 	if(root->data == ch)
	{
        	//printf("%c: ", root->data);
        	//printArr(arr, top);
		*l = top;
		for(int i = 0 ; i < top; i++)
		{
			code[i] = arr[i];
		}
	}
    }
}


void printLeaf(struct MinHeapNode* root, int arr[],
                int top)
{ 
    // Assign 0 to left edge and recur
    if (root->left) {
 
        arr[top] = 0;
        printLeaf(root->left, arr, top + 1);
    }
 
    // Assign 1 to right edge and recur
    if (root->right) {
 
        arr[top] = 1;
        printLeaf(root->right, arr, top + 1);
    }
 
    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root)) {
 
        printf("%c: ", root->data);
        printArr(arr, top);
    }
}

 
// The main function that builds a
// Huffman Tree and print codes by traversing
// the built Huffman Tree
void HuffmanCodes(char data[], int freq[], int size)
 
{
    // Construct Huffman Tree
    struct MinHeapNode* root
        = buildHuffmanTree(data, freq, size);
 
    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;
 
    //printCodes(root, arr, top);
}

void countLetter(char *filename, int freq[])
{
	char ch;
	FILE* input = fopen(filename, "r");
	while ((ch = fgetc(input)) != EOF)
	{
        	if(ch >= 'A' && ch <= 'Z')
		{
			freq[ch-'A']++;
		}
    	}
	fclose(input);
}

void buildHuffmanTableRecursive(struct MinHeapNode* root, int* table, int code, int codeLength) {
    if (!root->left && !root->right) {
        table[root->data] = code;
    }
    if (root->left) {
        buildHuffmanTableRecursive(root->left, table, (code << 1) | 0, codeLength + 1);
    }
    if (root->right) {
        buildHuffmanTableRecursive(root->right, table, (code << 1) | 1, codeLength + 1);
    }
}


// Function to compress a file based on the Huffman table
void compressFile(char* inputFile, char* outputFile, int* huffmanTable, int size)
{
    FILE* input = fopen(inputFile, "rb");
    FILE* output = fopen(outputFile, "wb");

    int bitBuffer = 0; // Buffer to store bits
    int bitCount = 0; // Number of bits in the buffer

    char ch;
    while ((ch = fgetc(input)) != EOF)
    {
        // Get the Huffman code for the character
        int code = huffmanTable[ch];
        // Write the bits of the code to the output file
        for (int i = 0; i < size; i++)
        {
            // Add the bit to the buffer
            bitBuffer <<= 1;
            bitBuffer |= ((code >> (size - 1 - i)) & 1);
            bitCount++;

            // Write the buffer to the output file if it is full
            if (bitCount == 8)
            {
                fputc(bitBuffer, output);
                bitBuffer = 0;
                bitCount = 0;
            }
        }
    }
    // Write the remaining bits in the buffer to the output file
    if (bitCount > 0)
    {
        bitBuffer <<= (8 - bitCount);
        fputc(bitBuffer, output);
    }

    fclose(input);
    fclose(output);
}

// Function to decompress a file based on the Huffman table
void decompressFile(char* inputFile, char* outputFile, int* huffmanTable, int size)
{
    FILE* input = fopen(inputFile, "rb");
    FILE* output = fopen(outputFile, "w");

    int bitBuffer = 0; // Buffer to store bits
    int bitCount = 0; // Number of bits in the buffer

    int ch;
    while ((ch = fgetc(input)) != EOF)
    {
        // Read each bit from the input file
        for (int i = 0; i < 8; i++)
        {
            // Add the bit to the buffer
            bitBuffer <<= 1;
            bitBuffer |= ((ch >> (7 - i)) & 1);
            bitCount++;

            // Check if the buffer contains a valid Huffman code
            if (bitCount >= size)
            {
                // Get the Huffman code from the buffer
                int code = 0;
                for (int j = 0; j < size; j++)
                {
                    code <<= 1;
                    code |= ((bitBuffer >> (size - 1 - j)) & 1);
                }

                // Find the character corresponding to the Huffman code
                for (int j = 0; j < size; j++)
                {
                    if (huffmanTable[j] == code)
                    {
			printf("%c", huffmanTable[j]);
                        // Write the character to the output file
                        fputc(j, output);
                        break;
                    }
                }

                // Reset the buffer and bit count
                bitBuffer = 0;
                bitCount = 0;
            }
        }
    }

    fclose(input);
    fclose(output);
}

int main()
{
	// We use two pipes
	// First pipe to send input string from parent
	// Second pipe to send concatenated string from child

	int fd1[2]; // Used to store two ends of first pipe
	int fd2[2]; // Used to store two ends of second pipe

	pid_t p;

	if (pipe(fd1)==-1)
	{
		fprintf(stderr, "Pipe Failed" );
		return 1;
	}
	if (pipe(fd2)==-1)
	{
		fprintf(stderr, "Pipe Failed" );
		return 1;
	}


	//download file terlebih dahulu
	char link[] = "https://drive.google.com/u/0/uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download";
	system("wget 'https://drive.google.com/u/0/uc?id=1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C&export=download' -O file.txt");

	p = fork();

	if (p < 0)
	{
		fprintf(stderr, "fork Failed" );
		return 1;
	}

	// Parent process
	else if (p > 0)
	{
		close(fd1[0]); 

		//ubah semua alphabet ke upper case
		system("tr a-z A-Z < file.txt > uppercase_file.txt");
                system("tr -cd '[:alpha:]' < uppercase_file.txt > new.txt");
                int freq[26] = {0};
		//hitung frekuensi
                countLetter("new.txt", freq);

		//tulis freq ke child
		write(fd1[1], &freq, sizeof(freq));
		close(fd1[1]);

		// Wait for child to send a string
		wait(NULL);

		close(fd2[1]); // Close writing end of second pipe


		//baca size table
		int size = 0;
		read(fd2[0], &size, sizeof(int));

		//read huffman table
		int table[size];
		read(fd2[0], &table, sizeof(int) * size);

		//decompress file
		decompressFile("compressed.txt", "decompressed.txt", table, size);
		close(fd2[0]);
	}

	// child process
	else
	{
		close(fd1[1]); // Close writing end of first pipe

		// Read a string using first pipe
		int freq[26];
		read(fd1[0], &freq, sizeof(freq));

		int count = 0;
		for(int i = 0; i < 26; i++)
                {
                        if(freq[i])count++;
                }

		int j = 0;
		int freq2[count];
                char alpha[count];

		//mencari huruf mana yang terdapat pada text
		for(int i = 0; i < 26; i++)
		{
			if(freq[i])
			{
				freq2[j] = freq[i];
				alpha[j] = 'A' + i;
				j++;
			}
		}


		//membuat huffman tree
		int size = sizeof(alpha) / sizeof(alpha[0]);
		struct MinHeapNode* root = buildHuffmanTree(alpha, freq2, size);
		int arr[MAX_TREE_HT], top = 0;

		printf("Size: %d\n", size);
		printf("Compressing File\n");


		int table[size];
		int temp = size;

		//membuat huffman table
		for (int i = 0; i < size; i++) {
        		table[i] = -1; // Initialize table entries to -1
    		}
    		buildHuffmanTableRecursive(root, table, 0, 0);

		//compress table
		int tablesize = sizeof(table) / sizeof(table[0]);
		compressFile("new.txt", "compressed.txt", table, tablesize);

		// Close both reading ends
		close(fd1[0]);
		close(fd2[0]);

		//print huffman code
		int arr2[MAX_TREE_HT], top2 = 0;
		printLeaf(root, arr2, top2);

		//write table ke parent
		write(fd2[1], &tablesize, sizeof(int));
		write(fd2[1], &table, sizeof(int) * tablesize);
		close(fd2[1]);

		exit(0);
	}
}
