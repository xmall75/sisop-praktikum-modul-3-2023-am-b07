#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <unistd.h>
#include <pthread.h>

void factorial(int angka) {
	unsigned long long ans = 1;
	for(int i = 2; i <= angka; i++)
	{
		ans *= i;
	}
	printf("%llu ", ans);
}

int main()
{
	key_t key = 1234;
	int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
	int (*res)[5];
	res = shmat(shmid, NULL, 0);

	printf("Hasil Matrix sisop.c : \n");
        printf("[");
        for(int i = 0; i < 4; i++)
        {
                printf("[");
                for(int j = 0; j < 5; j++)
                {
                        printf("%d", res[i][j]);
                        if(j != 4)printf(", ");
                }
                printf("]");
                if(i != 3)printf(", ");
        }
        printf("]\n");

	//hitung waktu
	clock_t t;
    	t = clock();


	//factorial
	for (int i=0 ; i<4; i++)
	{
		for(int j = 0; j < 5; j++)
		{
			factorial(res[i][j]);
		}
	}
	printf("\nProgram telah selesai\n");

	t = clock() - t;
    	double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    	printf("Waktu tidak menggunakan thread : %f detik\n", time_taken);

        shmdt(res);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}
