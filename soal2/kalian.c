#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int generateRandom(int lower, int upper)
{
	return (rand() % (upper - lower + 1)) + lower;
}


int main()
{
	key_t key = 1234;
	int matrix1[4][2];
	int matrix2[2][5];
	int (*matrixres)[5];

	int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
	matrixres = shmat(shmid, NULL, 0);
	//isi matrix 1
	srand(time(0));
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			matrix1[i][j] = generateRandom(1, 5);
		}
	}

        //isi matrix 2
        for(int i = 0; i < 2; i++)
        {
                for(int j = 0; j < 5; j++)
                {
                        matrix2[i][j] = generateRandom(1, 4);
                }
        }

	//perkalian matrix
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 5; j++)
		{
			matrixres[i][j] = 0;
			for(int k = 0; k < 2; k++)
			{
				matrixres[i][j] += matrix1[i][k] * matrix2[k][j];
			}
		}
	}

	//print matrix
	printf("Hasil Perkalian Program kalian.c : \n");
	printf("[");
	for(int i = 0; i < 4; i++)
	{
		printf("[");
		for(int j = 0; j < 5; j++)
		{
			printf("%d", matrixres[i][j]);
			if(j != 4)printf(", ");
		}
		printf("]");
		if(i != 3)printf(", ");
	}
	printf("]\n");
	shmdt(matrixres);
	return 0;
}
